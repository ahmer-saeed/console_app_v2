﻿using DBModel.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class NplDataFileLog : BaseEntity
    {
        public NplDataFileLog()
        {

        }

        public string FileName { get; set; }
        public double UploadDuration { get; set; }
        public string UploadStatus { get; set; }
        public int TotalRecord { get; set; }
        public string FileSource { get; set; }
        public string FileDestination { get; set; }
        public string FileDeleted { get; set; }
        public string FileDeleteExceptionMessage { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
