﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel.Base
{
    public class M_Department 
    {
        public M_Department()
        {

        }
        public int Id { get; set; }
        public int SortOrder { get; set; }
        public bool IsActive { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCode { get; set; }
    }
}
