﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class M_GenericStatus
    {
        public int Id { get; set; }
        public string StatusType { get; set; }
        public bool IsActive { get; set; }
    }
}
