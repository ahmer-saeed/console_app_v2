﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class DefaulterDetail
    {
        public DefaulterDetail()
        {

        }

        public int Id { get; set; }

        public int DefaulterId { get; set; }

        public string DirectorCIF { get; set; }
        public string DirectorName { get; set; }
        public string DirectorCnic { get; set; }
        public string DefaulterLocationOne { get; set; }
        public string DefaulterLocationTwo { get; set; }
        public string DefaulterLocationThree { get; set; }

        public Defaulter Defaulter { get; set; }
    }
}
