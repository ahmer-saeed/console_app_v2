﻿using DBModel.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class M_Currency
    {
        public int Id { get; set; }
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public int CountryId { get; set; }
        public M_Country Country { get; set; }
        public bool IsActive { get; set; }
    }
}
