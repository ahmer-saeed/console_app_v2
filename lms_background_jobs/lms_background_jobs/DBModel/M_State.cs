﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DBModel.Base
{
    public class M_State: BaseEntity
    {
        public M_State()
        {

        }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public M_Country Country { get; set; }
    }
}
