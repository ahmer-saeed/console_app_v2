﻿using DBModel.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class Defaulter : BaseEntity
    {
        public Defaulter()
        {
            DefaulterAccount = new List<DefaulterAccount>();
            DefaulterDetail = new List<DefaulterDetail>();
        }

        public string DefaulterCIF { get; set; }
        public string DefaulterName { get; set; }
        public string DefaulterType { get; set; }
        public string DefaulterSector { get; set; }
        public string DefaulterCnic { get; set; }
        public string DefaulterNtn { get; set; }
        public string DefaulterEcib { get; set; }
        public string DefaulterLocation { get; set; }
        public string ContactPerson { get; set; }
        public string DefaulterLocationTwo { get; set; }
        public string DefaulterLocationThree { get; set; }

        public List<DefaulterAccount> DefaulterAccount { get; set; }
        public List<DefaulterDetail> DefaulterDetail { get; set; }


    }
}
