﻿using System;

namespace DBModel.Base
{
    public interface IEntity
    {
        //int Id { get; set; }
        int  CreatedBy { get; set; }
        int ? UpdatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        DateTime? UpdatedOn { get; set; }

        /**
         * This field is used for Soft Delete
         * */
        bool IsActive { get; set; }

    }
}