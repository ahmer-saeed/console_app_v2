﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class LoanProductTypeDetail
    {
        public int Id { get; set; }
        public int LoanProductId { get; set; }
        public int LoanProductTypeId { get; set; }
        public M_LoanProductType LoanProductType { get; set; }
        public LoanProduct LoanProduct { get; set; }
    }
}
