﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel.Base
{
    public class M_DocumentType : BaseEntity
    {
        public M_DocumentType()
        {

        }
        public string DocumentName { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentDescription { get; set; }
        public int SortOrder { get; set; }
        public string DocumentTooltip { get; set; }
        public int? DocumentBelongToId { get; set; }
        public M_DocumentBelongTo DocumentBelongTo { get; set; }
    }
}
