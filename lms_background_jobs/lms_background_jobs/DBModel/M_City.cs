﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBModel.Base
{
    public class M_City : BaseEntity
    {
        public M_City()
        {
            City = new List<M_City>();
        }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public int? RegionId { get; set; }
        public M_Region Region { get; set; }
        public int StateId { get; set; }
        public M_State State { get; set; }

        [NotMapped]
        public List<M_City> City { get; set; }
    }
}
