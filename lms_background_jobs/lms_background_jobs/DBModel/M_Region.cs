﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel.Base
{
    public class M_Region : BaseEntity
    {
        public M_Region()
        {

        }
        public int SortOrder { get; set; }
        public string RegionName { get; set; }
        public string RegionCode { get; set; }
        public int CountryId { get; set; }
        public M_Country Country { get; set; }
    }
}
