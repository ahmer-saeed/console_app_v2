﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class LoanProduct
    {
        public LoanProduct()
        {
            LoanProductGroupDetail = new List<LoanProductGroupDetail>();
            LoanProductTypeDetail = new List<LoanProductTypeDetail>();
        }

        public int Id { get; set; }
        public string LoanProductCode { get; set; }
        public string LoanProductDescription { get; set; }
        public string LoanProductName { get; set; }
        public bool IsActive { get; set; }

        public List<LoanProductTypeDetail> LoanProductTypeDetail { get; set; }
        public List<LoanProductGroupDetail> LoanProductGroupDetail { get; set; }
    }
}
