﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel.Base
{
    public class M_Branch : BaseEntity
    {
        public M_Branch()
        {

        }
        public int SortOrder { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public int CityId { get; set; }
        public M_City City { get; set; }
    }
}
