﻿using DBModel.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class NplTemp
    {
        public NplTemp()
        {

        }

        public int Id { get; set; }
        public string DefaulterCIF { get; set; }
        public string DefaulterName { get; set; }
        public string DefaulterSector { get; set; }
        public string DefaulterCnic { get; set; }
        public string DefaulterNtn { get; set; }
        public string DefaulterEcib { get; set; }
        public string DefaulterLocation { get; set; }
        public string AccountNumber { get; set; }
        public string PrimaryCif { get; set; }
        public string BranchId { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string ProductDescription { get; set; }
        public string ClassificationType { get; set; }
        public string AccountOpeningDate { get; set; }
        public string DateOfDefault { get; set; }
        public string PrincipalAmount { get; set; }
        public string MarkupValue { get; set; }

    }
}
