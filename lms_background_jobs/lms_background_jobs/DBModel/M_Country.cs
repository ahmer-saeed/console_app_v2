﻿namespace DBModel.Base
{
    public class M_Country : BaseEntity
    {
        public M_Country()
        {

        }
        //public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}
