﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class M_LoanProductType
    {
        public int Id { get; set; }
        public string ProductTypeName { get; set; }
        public string ProductTypeCode { get; set; }
        public bool IsActive { get; set; }
    }
}
