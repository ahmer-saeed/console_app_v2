﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class M_LoanProductGroup
    {
        public int Id { get; set; }
        public string ProductGroupName { get; set; }
        public string ProductGroupCode { get; set; }
        public bool IsActive { get; set; }

        public LoanProductGroupDetail LoanProductGroupDetail { get; set; }
    }
}
