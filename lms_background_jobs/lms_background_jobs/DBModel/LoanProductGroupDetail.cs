﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class LoanProductGroupDetail
    {
        public int Id { get; set; }
        public int LoanProductId { get; set; }
        public LoanProduct LoanProduct { get; set; }
        public int LoanProductGroupId { get; set; }
        public M_LoanProductGroup LoanProductGroup { get; set; }
    }
}
