﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel.Base
{
    public class M_DocumentBelongTo : BaseEntity
    {
        public M_DocumentBelongTo()
        {

        }
        public string DocumentBelongTo { get; set; }
        public string DocumentBelongToCode { get; set; }
        public string DocumentBelongToDescription { get; set; }
        public int SortOrder { get; set; }
        public string DocumentBelongToTooltip { get; set; }
    }
}
