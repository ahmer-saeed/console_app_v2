﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class DefaulterFinance
    {
        public DefaulterFinance()
        {

        }
        public int Id { get; set; }
        public double PrincipalAmount { get; set; }
        public double MarkupPercentage { get; set; }
        public double MarkupValue { get; set; }
        public double TotalPenalty { get; set; }
        public string FinanceDescription { get; set; }
        public int DefaulterAccountId { get; set; }
        public DefaulterAccount DefaulterAccount { get; set; }

    }
}
