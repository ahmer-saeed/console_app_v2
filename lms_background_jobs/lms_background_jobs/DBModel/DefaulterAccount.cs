﻿
using DBModel.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBModel
{
    public class DefaulterAccount
    {
        public DefaulterAccount()
        {
        }
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public int? LoanProductId { get; set; }
        public int? BranchId { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public DateTime? AccountOpeningDate { get; set; }
        public DateTime DefaultDate { get; set; }
        public string ClassificationType { get; set; }
        public int DefaulterId { get; set; }
        public DateTime? AccountTransferDate { get; set; }
        public string ClassificationPercentage { get; set; }
        public bool? IsSecured { get; set; }
        public double RecoveredAmount { get; set; }
        public double RemainingAmount { get; set; }
        public int? CurrencyId { get; set; }
        public int? RegionId { get; set; }

        public M_Branch Branch { get; set; }
        public LoanProduct LoanProduct { get; set; }
        public M_Currency Currency { get; set; }
        public M_Region Region { get; set; }

        public Defaulter Defaulter { get; set; }
        public DefaulterFinance DefaulterFinance { get; set; }

    }
}
