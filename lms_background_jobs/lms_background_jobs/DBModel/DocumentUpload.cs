﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBModel.Base
{
    public class DocumentUpload : BaseEntity
    {
        public int? DocumentTypeId { get; set; }
        public M_DocumentType DocumentType { get; set; }
        public string RelativePath { get; set; }

        public string DocumentName { get; set; }
        public string DocumentExtension { get; set; }
        public string DocumentOriginalName { get; set; }

        [NotMapped]
        public System.IO.FileInfo File { get; set; }

        [NotMapped]
        public int? CaseBasicId { get; set; }

        [NotMapped]
        public int? LawFirmId { get; set; }

        [NotMapped]
        public int? LawFirmReferenceId { get; set; }

        [NotMapped]
        public int? UserId { get; set; }

        [NotMapped]
        public int? DefaulterId { get; set; }

        [NotMapped]
        public int? CaseCourtId { get; set; }

    }
}
