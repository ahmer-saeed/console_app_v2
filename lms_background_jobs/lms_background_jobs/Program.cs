﻿using DBModel.DBContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace DBModel
{
    class Program
    {
        //CIF No~CIF No ~CIF No|Defaulter Name~Defaulter Name ~Defaulter Name|
        //Defaulter Sector~Defaulter Sector ~Defaulter Sector|CNIC ~CNIC~CNIC|
        //NTN ~NTN~NTN|ECIB Code~ECIB Code ~ECIB Code|Official Location Address ~Official Location Address~Official Location Address|
        //Account Number|Primary CIF|BranchId|Product Group|Product Type|Product description|Classification Type|
        //Account Opening Date|Date of Default|PrincipalAmount|MarkupValue

        //0 - 20492|                                        =CIF 
        //1 - MUHAMMAD AYUB MUGAL|                          =Defaulter Nam
        //2 - |                                             =Defaulter Sector
        //3 - 42101-9055562-1|                              =CNIC
        //4 - |                                             =NTN
        //5 - |                                             =ECIB Code
        //6 - HOUSE# A-950 SECTOR 11-B TOWN SHIP NEW|       =Official Location Address
        //7 - 7000254678|                                   =Account Number
        //8 - 20492|                                        =Primary CIF
        //9 - 2|                                            =BranchId
        //10 - MTG|                                         =Product Group
        //11 - 7550|                                        =Product Type
        //12 - Saibaan Home Purchase|                       =Product description
        //13 - 104|                                         =Classification Type
        //14 - 59517|                                       =Account Opening Date
        //15 - |                                            =Date of Default
        //16 - 11675|                                       =PrincipalAmount
        //17 - 11565.61432                                  =MarkupValue

        public static string FormattedDate { get; set; }
        public static string FileBasePath = "docs";
        public static string FileTempPath = "tempfiles";
        public static string FileHistoryPath = "history";
        public static string NPLFile = "EXTLMS.txt";
        //public static string NPLFile = "*.txt";
        public static int TotalRecordInFile = 0;
        public static float Duration = 0;
        public static string FileUploadStatus = "NULL";
        public static string FileDeleteStatus = "NULL";

        //local
        //public static string FTPServer = "ftp://127.0.0.1:21";
        //public static string FTPUser = "SO-LPT-028";
        //public static string FTPPassword = "Skillorbit";

        //dev
        public static string FTPUser = "ftpuser";
        public static string FTPServer = "ftp://13.229.102.246:21";
        public static string FTPPassword = "skillorbit1!";

        //production
        //public static string FTPUser = "ftpuser1";
        //public static string FTPPassword = "skillorbit1!";
        //public static string FTPServer = "ftp://127.0.0.1:21";

        //uat
        //public static string FTPUser = "ftpuser1";
        //public static string FTPPassword = "skillorbit1!";
        //public static string FTPServer = "ftp://127.0.0.1:21";

        //var AbsoulteDirGeneral = Configuration["UploadDocument:FTPServer"]; //server path
        //var RelativeDirGeneral = Configuration["UploadDocument:RelativeDirGeneral"]; //docs
        //var RelativeFileDir = Configuration["UploadDocument:RelativeTempFileDir"]; //Directory for temp file

        //Email configuration
        public static string Host = "smtp.gmail.com";
        public static string UserName = "pakearn.com101@gmail.com";
        public static string Password = "asdf2018";
        public static string FromEmailAddress = "noreply@myblog.io";
        public static string DeliveryMethod = "smtp.gmail.com";
        public static bool EnableSSL = true;
        public static bool UseDefaultCredentials = false;
        public static int Port = 587;

        //Data related variables
        public static int LoanProductId { get; set; }
        public static int LoanProductGroupId { get; set; }
        public static DefaulterDetail DefaulterDetailNewRecord { get; set; }
        public static int DefaulterNewRecordId { get; set; }
        public static int DefaulterAccountNewRecordId { get; set; }
        public static string CompanyType { get; set; }
        public static string FileName { get; set; }
        public static string SourcePath { get; set; }
        public static string DestinationPath { get; set; }
        public static bool IsDirectorRecord = false;
        public static DocumentUploadContext context = new DocumentUploadContext();

        public static string FileUploadExceptionMessage { get; private set; }
        public static string FileDeleteExceptionMessage { get; private set; }
        public static Defaulter DefaulterNewRecord { get; set; }

        static void Main(string[] args)
        {
            //Startin stop watch
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.WriteLine(" ### Background App Start ### ");

            //Uploading Data
            //###########################

     //       var result = UploadFunction();

            //###########################

            //Stopping stop watch
            stopWatch.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            Duration = ts.Seconds;

            Console.WriteLine("### Elapsed Time in seconds " + Duration + " ####");


            //Insert Log
            var Log = new NplDataFileLog()
            {
                FileName = FileName,
                UploadDuration = Duration,
                UploadStatus = FileUploadStatus,
                TotalRecord = TotalRecordInFile,
                FileSource = ((SourcePath == "" || SourcePath == null) ? SourcePath : null),
                FileDestination = DestinationPath,
                FileDeleted = FileDeleteStatus,
                FileDeleteExceptionMessage = FileDeleteExceptionMessage,
                ExceptionMessage = FileUploadExceptionMessage,
                CreatedBy = 1,
                CreatedOn = DateTime.UtcNow,
                IsActive = true
            };

           // int LogId = AddNPLDataFileLogRecord(Log);

            Console.WriteLine("### Background App Finished ! ###");

            //Sending Email
            List<string> ToEmailAddress = new List<string>
            {
                "ahmer.saeed@skillorbit.com"
            };

            List<string> EmailBody = new List<string>
            {
                "ahmer.saeed@skillorbit.com"
            };


            //Sending Email
            //  SendEmail(ToEmailAddress, "Upload NPL Data via File", EmailBody);

            //  Console.ReadKey();
            //Environment.Exit(0);
        }

        public static string UploadFunctionOld()
        {
            SourcePath = FTPServer + "/" + FileBasePath + "/" + FileTempPath + "/" + NPLFile;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(SourcePath);
            request.Credentials = new NetworkCredential(FTPUser, FTPPassword);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.UseBinary = true;
            request.KeepAlive = false;
            request.Timeout = -1;
            request.UsePassive = true;

            //checking number of records in the file

            try
            {
                Stream reader = request.GetResponse().GetResponseStream();

                //Reading each line to upload record in database
                using (StreamReader sr = new StreamReader(reader))
                {
                    string line;

                    //Reading uploaded document line by line
                    while ((line = sr.ReadLine()) != null)
                    {
                        TotalRecordInFile += 1;
                        //Resolving Loan Product data
                        if (line != null && line != "")
                        {
                            string[] ReadLine = line.Split("|");

                            //checking LoanProductCode
                            if (ReadLine[11].Trim() != null)
                            {
                                var CheckLoanProduct = GetLoanProductIfExist(ReadLine[11].Trim());
                                if (CheckLoanProduct > 0)
                                {
                                    //use existing loan product code
                                    LoanProductId = CheckLoanProduct;
                                }
                                else
                                {
                                    //add new loan product code
                                    LoanProductId = AddLoanProduct(ReadLine);
                                }
                            }

                            //checking MLoanProductGroup and LoanProductGroupDetail
                            if (ReadLine[10].Trim() != null)
                            {
                                var checkLoanProductGroup = GetLoanProductGroupIfExist(ReadLine[10].Trim());

                                if (checkLoanProductGroup > 0)
                                {
                                    //use existing loan product group
                                    LoanProductGroupId = checkLoanProductGroup;
                                }
                                else
                                {
                                    //add new M_LoanProductGroup and LoanProductGroupDetail
                                    LoanProductGroupId = AddMLoanProductGroupAndLoanProductGroupDetail(ReadLine, LoanProductId);
                                }
                            }

                            //core business logics - START 
                            //i -e adding defautler, defaulter account,
                            //defaulter finance and defautler details records
                            if (ReadLine[0].Trim().Contains("~"))
                            {
                                IsDirectorRecord = true;
                                CompanyType = "Company";

                                string[] multipleCIF = ReadLine[0].Trim().Split("~");

                                int count = 0;

                                if (multipleCIF.Count() > 0)
                                {
                                    foreach (var cif in multipleCIF)
                                    {
                                        if (count != multipleCIF.Count() - 1)
                                        {
                                            string[] directorName = ReadLine[1].Trim().Split("~");
                                            string[] directorCnic = ReadLine[3].Trim().Split("~");

                                            //compairing cif number with primary cif
                                            //to check it is Director Record of 
                                            //Defaulter record
                                            if (cif != ReadLine[8].Trim())
                                            {
                                                //means cif is not equals to primary cif
                                                //i-e it is a DIRECTOR record 
                                                //so add it in DefaulterDetail table    

                                                DefaulterDetailNewRecord = new DefaulterDetail()
                                                {
                                                    DirectorCIF = cif,
                                                    DirectorName = directorName[count].Trim(),
                                                    DirectorCnic = directorCnic[count].Trim()
                                                };
                                            }
                                            else
                                            {
                                                //means cif is equals to primary cif
                                                //i-e it is a Defaulter record 
                                                //so add it in Defaulter table 
                                                //only it this Defaulter is not exist 
                                                //in the database other wise update it

                                                Defaulter DefaulterRecord = GetDefaulterRecordIfExist(ReadLine[8].Trim());
                                                if (DefaulterRecord != null)
                                                {
                                                    //means Defaulter records exist

                                                    //means Defaulter records not exist
                                                    string[] DefaulterSector = ReadLine[2].Trim().Split("~");
                                                    string[] DefaulterNtn = ReadLine[4].Trim().Split("~");
                                                    string[] DefaulterEcib = ReadLine[5].Trim().Split("~");
                                                    string[] DefaulterLocation = ReadLine[6].Trim().Split("~");

                                                    //Adding new Defaulter record

                                                    DefaulterRecord.DefaulterName = directorName[count].Trim();
                                                    DefaulterRecord.DefaulterType = CompanyType;
                                                    DefaulterRecord.DefaulterSector = DefaulterSector[0];
                                                    DefaulterRecord.DefaulterCnic = directorCnic[count].Trim();
                                                    DefaulterRecord.DefaulterNtn = DefaulterNtn[0];
                                                    DefaulterRecord.DefaulterEcib = DefaulterEcib[0];
                                                    DefaulterRecord.DefaulterLocation = DefaulterLocation[0];
                                                    DefaulterRecord.UpdatedBy = 1; //bind super admin user id here
                                                    DefaulterRecord.UpdatedOn = DateTime.UtcNow;

                                                    var UpdateDefaulterRecord = UpdateDefaulterData(DefaulterRecord);
                                                }
                                                else
                                                {
                                                    //means Defaulter records not exist

                                                    string[] DefaulterSector = ReadLine[2].Trim().Split("~");
                                                    string[] DefaulterNtn = ReadLine[4].Trim().Split("~");
                                                    string[] DefaulterEcib = ReadLine[5].Trim().Split("~");
                                                    string[] DefaulterLocation = ReadLine[6].Trim().Split("~");

                                                    //Update existing Defaulter record
                                                    Defaulter DefaulterNewRecord = new Defaulter()
                                                    {
                                                        DefaulterCIF = ReadLine[8].Trim(),
                                                        DefaulterName = directorName[count].Trim(),
                                                        DefaulterType = CompanyType,
                                                        DefaulterSector = DefaulterSector[0],
                                                        DefaulterCnic = directorCnic[count].Trim(),
                                                        DefaulterNtn = DefaulterNtn[0],
                                                        DefaulterEcib = DefaulterEcib[0],
                                                        DefaulterLocation = DefaulterLocation[0],
                                                        CreatedBy = 1, //bind super admin user id here
                                                        CreatedOn = DateTime.UtcNow,
                                                        IsActive = true
                                                    };
                                                    DefaulterNewRecordId = AddDefaulterData(DefaulterNewRecord);
                                                }

                                                List<DefaulterDetail> DefaulterDetailRecord = GetDefaulterDetailRecordIfExist(DefaulterRecord.Id);
                                                if (DefaulterDetailRecord.Count() > 0)
                                                {
                                                    //means DefaulterDetail records exist
                                                    RemoveDefaulterDetailData(DefaulterDetailRecord);

                                                    //Adding new DefaulterDetail record
                                                    int DefaulterDetailNewRecordId = AddDefaulterDetailData(DefaulterDetailNewRecord);
                                                }
                                                else
                                                {
                                                    //means DefaulterDetail records not exist
                                                    //Adding new DefaulterDetail record
                                                    int DefaulterDetailNewRecordId = AddDefaulterDetailData(DefaulterDetailNewRecord);
                                                }
                                            }
                                            count++;
                                        }
                                    }
                                }
                            }

                            if (!IsDirectorRecord)
                            {
                                CompanyType = "Individual";

                                Defaulter DefaulterRecord = GetDefaulterRecordIfExist(ReadLine[8].Trim());
                                if (DefaulterRecord != null)
                                {
                                    //means Defaulter records exist

                                    //Updating existing Defaulter record
                                    DefaulterRecord.DefaulterName = ReadLine[1].Trim();
                                    DefaulterRecord.DefaulterType = CompanyType;
                                    DefaulterRecord.DefaulterSector = ReadLine[2].Trim();
                                    DefaulterRecord.DefaulterCnic = ReadLine[3].Trim();
                                    DefaulterRecord.DefaulterEcib = ReadLine[5].Trim();
                                    DefaulterRecord.DefaulterLocation = ReadLine[6].Trim();
                                    DefaulterRecord.UpdatedBy = 1; //bind super admin user id here
                                    DefaulterRecord.UpdatedOn = DateTime.UtcNow;

                                    var UpdateDefaulterRecord = UpdateDefaulterData(DefaulterRecord);
                                }
                                else
                                {
                                    //means Defaulter records not exist

                                    //Adding new Defaulter record
                                    DefaulterNewRecord = new Defaulter()
                                    {
                                        DefaulterCIF = ReadLine[8].Trim(),
                                        DefaulterName = ReadLine[1].Trim(),
                                        DefaulterType = CompanyType,
                                        DefaulterSector = ReadLine[2].Trim(),
                                        DefaulterCnic = ReadLine[3].Trim(),
                                        DefaulterEcib = ReadLine[5].Trim(),
                                        DefaulterLocation = ReadLine[6].Trim(),
                                        CreatedBy = 1, //bind super admin user id here
                                        CreatedOn = DateTime.UtcNow,
                                        IsActive = true
                                    };
                                    DefaulterNewRecordId = AddDefaulterData(DefaulterNewRecord);
                                }
                            }

                            DefaulterAccount DefaulterAccountRecord = GetDefaulterAccountRecordIfExist(ReadLine[7].Trim());
                            if (DefaulterAccountRecord != null)
                            {
                                //means DefaulterAccount records exist

                                //Updating existing DefaulterAccount record
                                DefaulterAccountRecord.BranchId = int.Parse(ReadLine[9].Trim());
                                DefaulterAccountRecord.LoanProductId = LoanProductId;
                                DefaulterAccountRecord.ClassificationType = ReadLine[13].Trim();
                                DefaulterAccountRecord.AccountOpeningDate = DateTime.UtcNow;
                                DefaulterAccountRecord.DefaultDate = DateTime.UtcNow;

                                DefaulterNewRecord.DefaulterAccount.Add(DefaulterAccountRecord);

                                //var UpdateDefaulterAccountRecord = UpdateDefaulterAccountData(DefaulterAccountRecord);
                            }
                            else
                            {
                                //means DefaulterAccount records not exist
                                //Adding new DefaulterAccount record
                                DefaulterAccount DefaulterAccountNewRecord = new DefaulterAccount()
                                {
                                    AccountNumber = ReadLine[7].Trim(),
                                    BranchId = int.Parse(ReadLine[9].Trim()),
                                    LoanProductId = LoanProductId,
                                    ClassificationType = ReadLine[13].Trim(),
                                    AccountOpeningDate = DateTime.UtcNow,
                                    DefaultDate = DateTime.UtcNow,
                                    DefaulterId = DefaulterNewRecordId,
                                    CurrencyId = 1, // use PKR currency later on
                                    AccountTransferDate = DateTime.UtcNow,
                                    IsSecured = true,
                                    RecoveredAmount = 1000,
                                    RemainingAmount = 500,
                                    RegionId = 1
                                };

                                DefaulterNewRecord.DefaulterAccount.Add(DefaulterAccountNewRecord);

                                //DefaulterAccountNewRecordId = AddDefaulterAccountData(DefaulterAccountNewRecord);
                            }

                            //DefaulterFinance DefaulterFinanceRecord = GetDefaulterFinanceRecordIfExist(ReadLine[7].Trim());
                            //if (DefaulterFinanceRecord != null)
                            //{
                            //    //means DefaulterFinanceRecord records exist

                            //    //Updating existing DefautlerFinanceRecord
                            //    DefaulterFinanceRecord.PrincipalAmount = Convert.ToDouble(ReadLine[16].Trim());
                            //    DefaulterFinanceRecord.MarkupValue = Convert.ToDouble(ReadLine[17].Trim());

                            //    //var UpdateDefaulterFinanceRecord = UpdateDefaulterFinanceData(DefaulterFinanceRecord);
                            //}
                            //else
                            //{
                            //    //means DefaulterFinanceRecord records not exist
                            //    //Adding new DefaulterFinance record
                            //    DefaulterFinance DefaulterFinanceNewRecord = new DefaulterFinance()
                            //    {
                            //        PrincipalAmount = Convert.ToDouble(ReadLine[16].Trim()),
                            //        MarkupValue = Convert.ToDouble(ReadLine[17].Trim()),
                            //        DefaulterAccountId = DefaulterAccountNewRecordId,
                            //    };
                            //    int DefaulterFinanceNewRecordNewRecordId = AddDefaulterFinanceData(DefaulterFinanceNewRecord);
                            //}

                            IsDirectorRecord = false;
                        }
                        else
                        {
                            FileUploadStatus = "NPL File has no Records";
                        }
                        context.SaveChanges();
                    }
                }

                FileUploadStatus = "NPL File Records are uploaded";


            }
            catch (WebException e)
            {
                String status = ((FtpWebResponse)e.Response).StatusDescription;
            }
            //catch (Exception ex)
            //{
            //    FileUploadExceptionMessage = ex.Message;
            //    return FileUploadExceptionMessage;
            //}

            return FileUploadStatus;
        }

        public static string UploadFunction()
        {
            try
            {
                SourcePath = FTPServer + "/" + FileBasePath + "/" + FileTempPath + "/" + NPLFile;

                DestinationPath = FTPServer + "/" + FileBasePath + "/" + FileHistoryPath + "/" + "_" + NPLFile;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(SourcePath);
                request.Credentials = new NetworkCredential(FTPUser, FTPPassword);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.KeepAlive = false;
                request.Timeout = -1;
                request.UsePassive = false;

                using (Stream stream = request.GetResponse().GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        while (stream.CanRead == true)
                        {
                            try
                            {
                                string line = reader.ReadLine();

                                //counting lines
                                TotalRecordInFile += 1;

                                // process the line
                                Console.WriteLine(line);

                                //Resolving Loan Product data
                                if (line != null && line != "")
                                {
                                    string[] ReadLine = line.Split("|");

                                    //checking LoanProductCode
                                    if (ReadLine[11].Trim() != null)
                                    {
                                        var CheckLoanProduct = GetLoanProductIfExist(ReadLine[11].Trim());
                                        if (CheckLoanProduct > 0)
                                        {
                                            //use existing loan product code
                                            LoanProductId = CheckLoanProduct;
                                        }
                                        else
                                        {
                                            //add new loan product code
                                            LoanProductId = AddLoanProduct(ReadLine);
                                        }
                                    }

                                    //checking MLoanProductGroup and LoanProductGroupDetail
                                    if (ReadLine[10].Trim() != null)
                                    {
                                        var GetLoanProductGroupId = GetLoanProductGroupIfExist(ReadLine[10].Trim());
                                        if (GetLoanProductGroupId > 0)
                                        {
                                            //use existing loan product group
                                            LoanProductGroupId = GetLoanProductGroupId;
                                        }
                                        else
                                        {
                                            //add new M_LoanProductGroup and LoanProductGroupDetail
                                            LoanProductGroupId = AddMLoanProductGroupAndLoanProductGroupDetail(ReadLine, LoanProductId);
                                        }
                                    }

                                    //core business logics - START 
                                    //i -e adding defautler, defaulter account,
                                    //defaulter finance and defautler details records
                                    if (ReadLine[0].Trim().Contains("~"))
                                    {
                                        IsDirectorRecord = true;
                                        CompanyType = "Company";

                                        string[] multipleCIF = ReadLine[0].Trim().Split("~");

                                        int count = 0;

                                        if (multipleCIF.Count() > 0)
                                        {
                                            foreach (var cif in multipleCIF)
                                            {
                                                if (count <= multipleCIF.Count() - 1)
                                                {
                                                    string[] directorName = ReadLine[1].Trim().Split("~");
                                                    string[] directorCnic = ReadLine[3].Trim().Split("~");
                                                    string[] directorLocation = ReadLine[6].Trim().Split("~");

                                                    //compairing cif number with primary cif
                                                    //to check it is Director Record of 
                                                    //Defaulter record
                                                    if (cif != ReadLine[8].Trim())
                                                    {
                                                        //means cif is not equals to primary cif
                                                        //i-e it is a DIRECTOR record 
                                                        //so add it in DefaulterDetail table    

                                                        DefaulterDetailNewRecord = new DefaulterDetail()
                                                        {
                                                            DirectorCIF = cif,
                                                            DirectorName = directorName[count].Trim(),
                                                            DirectorCnic = directorCnic[count].Trim(),
                                                            DefaulterLocationOne = directorLocation[count].Trim()
                                                        };
                                                        DefaulterNewRecord.DefaulterDetail.Add(DefaulterDetailNewRecord);
                                                    }
                                                    else
                                                    {
                                                        //means cif is equals to primary cif
                                                        //i-e it is a Defaulter record 
                                                        //so add it in Defaulter table 
                                                        //only it this Defaulter is not exist 
                                                        //in the database other wise update it

                                                        DefaulterNewRecord = GetDefaulterRecordIfExist(ReadLine[8].Trim());
                                                        if (DefaulterNewRecord != null)
                                                        {
                                                            //means Defaulter records exist

                                                            //means Defaulter records not exist
                                                            string[] DefaulterSector = ReadLine[2].Trim().Split("~");
                                                            string[] DefaulterNtn = ReadLine[4].Trim().Split("~");
                                                            string[] DefaulterEcib = ReadLine[5].Trim().Split("~");
                                                            string[] DefaulterLocation = ReadLine[6].Trim().Split("~");

                                                            //Adding new Defaulter record

                                                            DefaulterNewRecord.DefaulterName = directorName[count].Trim();
                                                            DefaulterNewRecord.DefaulterType = CompanyType;
                                                            DefaulterNewRecord.DefaulterSector = DefaulterSector[0];
                                                            DefaulterNewRecord.DefaulterCnic = directorCnic[count].Trim();
                                                            DefaulterNewRecord.DefaulterNtn = DefaulterNtn[0];
                                                            DefaulterNewRecord.DefaulterEcib = DefaulterEcib[0];
                                                            DefaulterNewRecord.DefaulterLocation = DefaulterLocation[0];
                                                            DefaulterNewRecord.UpdatedBy = 1; //bind super admin user id here
                                                            DefaulterNewRecord.UpdatedOn = DateTime.UtcNow;

                                                            var UpdateDefaulterRecord = UpdateDefaulterData(DefaulterNewRecord);
                                                        }
                                                        else
                                                        {
                                                            //means Defaulter records not exist

                                                            string[] DefaulterSector = ReadLine[2].Trim().Split("~");
                                                            string[] DefaulterNtn = ReadLine[4].Trim().Split("~");
                                                            string[] DefaulterEcib = ReadLine[5].Trim().Split("~");
                                                            string[] DefaulterLocation = ReadLine[6].Trim().Split("~");

                                                            //Update existing Defaulter record
                                                            DefaulterNewRecord = new Defaulter()
                                                            {
                                                                DefaulterCIF = ReadLine[8].Trim(),
                                                                DefaulterName = directorName[count].Trim(),
                                                                DefaulterType = CompanyType,
                                                                DefaulterSector = DefaulterSector[0],
                                                                DefaulterCnic = directorCnic[count].Trim(),
                                                                DefaulterNtn = DefaulterNtn[0],
                                                                DefaulterEcib = DefaulterEcib[0],
                                                                DefaulterLocation = DefaulterLocation[0],
                                                                CreatedBy = 1, //bind super admin user id here
                                                                CreatedOn = DateTime.UtcNow,
                                                                IsActive = true
                                                            };
                                                            DefaulterNewRecordId = AddDefaulterData(DefaulterNewRecord);
                                                        }

                                                        List<DefaulterDetail> DefaulterDetailRecord = GetDefaulterDetailRecordIfExist(DefaulterNewRecord.Id);
                                                        if (DefaulterDetailRecord.Count() > 0)
                                                        {
                                                            //means DefaulterDetail records exist
                                                            RemoveDefaulterDetailData(DefaulterDetailRecord);
                                                        }
                                                    }
                                                    count++;
                                                }
                                            }
                                        }
                                    }

                                    if (!IsDirectorRecord)
                                    {
                                        CompanyType = "Individual";

                                        DefaulterNewRecord = GetDefaulterRecordIfExist(ReadLine[8].Trim());
                                        if (DefaulterNewRecord != null)
                                        {
                                            //means Defaulter records exist

                                            //Updating existing Defaulter record
                                            DefaulterNewRecord.DefaulterName = ReadLine[1].Trim();
                                            DefaulterNewRecord.DefaulterType = CompanyType;
                                            DefaulterNewRecord.DefaulterSector = ReadLine[2].Trim();
                                            DefaulterNewRecord.DefaulterCnic = ReadLine[3].Trim();
                                            DefaulterNewRecord.DefaulterEcib = ReadLine[5].Trim();
                                            DefaulterNewRecord.DefaulterLocation = ReadLine[6].Trim();
                                            DefaulterNewRecord.UpdatedBy = 1; //bind super admin user id here
                                            DefaulterNewRecord.UpdatedOn = DateTime.UtcNow;

                                            var UpdateDefaulterRecord = UpdateDefaulterData(DefaulterNewRecord);
                                        }
                                        else
                                        {
                                            //means Defaulter records not exist

                                            //Adding new Defaulter record
                                            DefaulterNewRecord = new Defaulter()
                                            {
                                                DefaulterCIF = ReadLine[8].Trim(),
                                                DefaulterName = ReadLine[1].Trim(),
                                                DefaulterType = CompanyType,
                                                DefaulterSector = ReadLine[2].Trim(),
                                                DefaulterCnic = ReadLine[3].Trim(),
                                                DefaulterEcib = ReadLine[5].Trim(),
                                                DefaulterLocation = ReadLine[6].Trim(),
                                                CreatedBy = 1, //bind super admin user id here
                                                CreatedOn = DateTime.UtcNow,
                                                IsActive = true
                                            };
                                            DefaulterNewRecordId = AddDefaulterData(DefaulterNewRecord);
                                        }
                                    }

                                    DefaulterAccount DefaulterAccountRecord = GetDefaulterAccountRecordIfExist(ReadLine[7].Trim());

                                    if (DefaulterAccountRecord != null)
                                    {
                                        //means DefaulterAccount records exist

                                        //Updating existing DefaulterAccount record
                                        DefaulterAccountRecord.BranchId = int.Parse(ReadLine[9].Trim());
                                        DefaulterAccountRecord.LoanProductId = LoanProductId;
                                        DefaulterAccountRecord.ClassificationType = ReadLine[13].Trim();
                                        DefaulterAccountRecord.AccountOpeningDate = DateTime.UtcNow;
                                        DefaulterAccountRecord.DefaultDate = DateTime.UtcNow;

                                        //Removing existing defaulter account record 
                                        //RemoveDefaulterAccountData(DefaulterAccountRecord);

                                        DefaulterNewRecord.DefaulterAccount.Add(DefaulterAccountRecord);
                                    }
                                    else
                                    {
                                        //means DefaulterAccount records not exist
                                        //Adding new DefaulterAccount record
                                        DefaulterAccount DefaulterAccountNewRecord = new DefaulterAccount()
                                        {
                                            AccountNumber = ReadLine[7].Trim(),
                                            BranchId = int.Parse(ReadLine[9].Trim()),
                                            LoanProductId = LoanProductId,
                                            ClassificationType = ReadLine[13].Trim(),
                                            AccountOpeningDate = DateTime.UtcNow,
                                            DefaultDate = DateTime.UtcNow,
                                            DefaulterId = DefaulterNewRecordId,
                                            CurrencyId = 1, // use PKR currency later on
                                            AccountTransferDate = DateTime.UtcNow,
                                            IsSecured = true,
                                            RecoveredAmount = 1000,
                                            RemainingAmount = 500,
                                            RegionId = 1
                                        };

                                        DefaulterNewRecord.DefaulterAccount.Add(DefaulterAccountNewRecord);
                                    }

                                    DefaulterFinance DefaulterFinanceRecord = GetDefaulterFinanceRecordIfExist(ReadLine[7].Trim());
                                    if (DefaulterFinanceRecord != null)
                                    {
                                        //means DefaulterFinanceRecord records exist

                                        //Updating existing DefautlerFinanceRecord
                                        DefaulterFinanceRecord.PrincipalAmount = Convert.ToDouble(ReadLine[16].Trim());
                                        DefaulterFinanceRecord.MarkupValue = Convert.ToDouble(ReadLine[17].Trim());

                                        DefaulterNewRecord.DefaulterAccount[0].DefaulterFinance = DefaulterFinanceRecord;
                                    }
                                    else
                                    {
                                        //means DefaulterFinanceRecord records not exist
                                        //Adding new DefaulterFinance record
                                        DefaulterFinance DefaulterFinanceNewRecord = new DefaulterFinance()
                                        {
                                            PrincipalAmount = Convert.ToDouble(ReadLine[16].Trim()),
                                            MarkupValue = Convert.ToDouble(ReadLine[17].Trim()),
                                            //DefaulterAccountId = DefaulterAccountNewRecordId,
                                        };
                                        DefaulterNewRecord.DefaulterAccount[0].DefaulterFinance = DefaulterFinanceNewRecord;
                                    }
                                    IsDirectorRecord = false;
                                }

                                //committing transaction
                                try
                                {
                                    context.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    FileUploadExceptionMessage = ex.Message;
                                    return FileUploadExceptionMessage;
                                }
                            }
                            catch (WebException e)
                            {
                                FileUploadExceptionMessage = ((FtpWebResponse)e.Response).StatusDescription;
                                return FileUploadExceptionMessage;
                            }
                        }
                    }
                }

                FileUploadStatus = "NPL File Records are uploaded";

                //Now move the file to History directroy
                //try
                //{
                //    FileDeleteStatus = "Start Delete"


                //    FileDeleteStatus = "File Deleted";
                //}
                //catch (Exception ex)
                //{
                //    FileDeleteExceptionMessage = ex.Message;
                //    return FileDeleteExceptionMessage;
                //}

                return FileUploadStatus;
            }
            catch (WebException e)
            {
                FileUploadStatus = ((FtpWebResponse)e.Response).StatusDescription;
                return FileUploadStatus;
            }
        }

        public static bool SendEmail(List<string> to, string subject, List<string> body)
        {
            SmtpClient client = new SmtpClient(Host, Port)
            {
                UseDefaultCredentials = UseDefaultCredentials,
                EnableSsl = EnableSSL,
                Credentials = new NetworkCredential(UserName, Password)
            };

            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(FromEmailAddress)
            };

            for (int i = 0; i < to.Count(); i++)
            {
                mailMessage.To.Add(to[i]);
            }

            mailMessage.Subject = subject;
            mailMessage.Body = body[0];//bind file status, file exception, file move status , file move exception , duration, and total records
            mailMessage.IsBodyHtml = true;

            try
            {
                Console.WriteLine("## Sending Email ##");

                try
                {
                    client.Send(mailMessage);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Email Exception: " + ex);
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to Send Email, Error Message, " + ex);
                return false;
            }
        }

        public static int AddNplTempRange(List<NplTemp> nplTemp)
        {
            context.NplTemp.AddRange(nplTemp);
            return 0;
        }

        public static int AddDefaulterData(Defaulter defaulterNewRecord)
        {
            context.Defaulter.Add(defaulterNewRecord);
            return 0;

            //using (var context = new DBContext.DocumentUploadContext())
            //{

            //context.SaveChanges();

            //if (defaulterNewRecord.Id != 0)
            //{
            //    return defaulterNewRecord.Id;
            //}
            //else
            //{
            //    return 0;
            //}
        }

        public static string UpdateDefaulterData(Defaulter defaulterExistingRecord)
        {
            //using (var context = new DBContext.DocumentUploadContext())
            //{
            //    context.Defaulter.Update(defaulterExistingRecord);
            //    //context.SaveChanges();
            //}

            var update = context.Defaulter.Update(defaulterExistingRecord);
            return "true";
        }

        public static int AddDefaulterAccountData(DefaulterAccount defaulterAccountNewRecord)
        {
            //using (context = new DBContext.DocumentUploadContext())
            //{

            //}

            //context.SaveChanges();

            context.DefaulterAccount.Add(defaulterAccountNewRecord);

            if (defaulterAccountNewRecord.Id != 0)
            {
                return defaulterAccountNewRecord.Id;
            }
            else
            {
                return 0;
            }
        }

        public static string UpdateDefaulterAccountData(DefaulterAccount defautlerAccountExistingRecord)
        {
            //using (var context = new DBContext.DocumentUploadContext())
            //{
            //    context.DefaulterAccount.Update(defautlerAccountExistingRecord);
            //    //context.SaveChanges();
            //}

            var update = context.DefaulterAccount.Update(defautlerAccountExistingRecord);

            return "true";
        }

        public static int AddDefaulterDetailData(DefaulterDetail defaulterDetailNewRecord)
        {
            context.DefaulterDetail.Add(defaulterDetailNewRecord);

            if (defaulterDetailNewRecord.Id != 0)
            {
                return defaulterDetailNewRecord.Id;
            }
            else
            {
                return 0;
            }
        }

        public static string UpdateDefaulterDetailData(DefaulterDetail defaulterDetailExistingRecord)
        {
            //using (var context = new DBContext.DocumentUploadContext())
            //{
            //    context.DefaulterDetail.Update(defaulterDetailExistingRecord);
            //    //context.SaveChanges();
            //}

            var update = context.DefaulterDetail.Update(defaulterDetailExistingRecord);
            return "true";
        }

        public static int AddDefaulterFinanceData(DefaulterFinance defaulterFinanceNewRecord)
        {
            //using (context = new DBContext.DocumentUploadContext())
            //{
            //    context.DefaulterFinance.Add(defaulterFinanceNewRecord);
            //}

            //context.DefaulterFinance.Add(defaulterFinanceNewRecord);

            if (defaulterFinanceNewRecord.Id != 0)
            {
                return defaulterFinanceNewRecord.Id;
            }
            else
            {
                return 0;
            }
        }

        public static string UpdateDefaulterFinanceData(DefaulterFinance defaulterFinanceExistingRecord)
        {
            var update = context.DefaulterFinance.Update(defaulterFinanceExistingRecord);
            return "true";
        }

        public static string RemoveDefaulterAccountData(DefaulterAccount defaulterAccount)
        {
            context.DefaulterAccount.RemoveRange(defaulterAccount);
            return "true";
        }

        public static string RemoveDefaulterDetailData(List<DefaulterDetail> defaulterDetail)
        {
            context.DefaulterDetail.RemoveRange(defaulterDetail);
            return "true";
        }

        public static int AddLoanProduct(string[] loanProduct)
        {
            var std = new LoanProduct()
            {
                LoanProductCode = loanProduct[11].Trim(),
                LoanProductDescription = loanProduct[12].Trim(),
                LoanProductName = loanProduct[12].Trim(),
                IsActive = true
            };

            context.LoanProduct.Add(std);

            return std.Id;
        }

        public static int AddMLoanProductGroupAndLoanProductGroupDetail(string[] mloanProductGroup, int LoanProductId)
        {
            var LoanProductGroup = new M_LoanProductGroup()
            {
                ProductGroupCode = mloanProductGroup[10].Trim(),
                ProductGroupName = mloanProductGroup[10].Trim(),
                IsActive = true
            };

            var LoanProductGroupDetail = new LoanProductGroupDetail()
            {
                LoanProductId = LoanProductId,
            };

            LoanProductGroup.LoanProductGroupDetail = LoanProductGroupDetail;

            context.M_LoanProductGroup.Add(LoanProductGroup);

            return LoanProductGroup.Id;
        }

        public static Defaulter GetDefaulterRecordIfExist(string DefaulterCif)
        {
            var DefaulterRecord = context.Defaulter
                                              .Where(s => s.DefaulterCIF == DefaulterCif)
                                              .FirstOrDefault();

            if (DefaulterRecord != null)
            {
                return DefaulterRecord;
            }
            else
            {
                return null;
            }
        }

        public static List<DefaulterDetail> GetDefaulterDetailRecordIfExist(int DefaulterId)
        {
            var DefaulterDetailRecord = context.DefaulterDetail
                                              .Where(s => s.DefaulterId == DefaulterId)
                                              .ToList();

            if (DefaulterDetailRecord.Count() != 0)
            {
                return DefaulterDetailRecord;
            }
            else
            {
                return DefaulterDetailRecord;
            }
        }

        public static DefaulterFinance GetDefaulterFinanceRecordIfExist(string DefaulterAccountNumber)
        {
            var DefaulterFinanceRecord = context.DefaulterFinance
                                   .Include(x => x.DefaulterAccount)
                                  .Where(s => s.DefaulterAccount.AccountNumber == DefaulterAccountNumber)
                                  .FirstOrDefault();

            if (DefaulterFinanceRecord != null)
            {
                return DefaulterFinanceRecord;
            }
            else
            {
                return null;
            }
        }

        public static DefaulterAccount GetDefaulterAccountRecordIfExist(string accountNumber)
        {
            var DefaulterAccountRecord = context.DefaulterAccount
                                              .Where(s => s.AccountNumber == accountNumber)
                                              .FirstOrDefault();

            if (DefaulterAccountRecord != null)
            {
                return DefaulterAccountRecord;
            }
            else
            {
                return null;
            }
        }

        public static int GetLoanProductIfExist(string loanProductCode)
        {
            var LoanProductData = context.LoanProduct
                                              .Where(s => s.LoanProductCode == loanProductCode)
                                              .Where(s => s.LoanProductCode == loanProductCode)
                                              .FirstOrDefault();

            if (LoanProductData != null)
            {
                return LoanProductData.Id;
            }
            else
            {
                return 0;
            }
        }

        public static int GetLoanProductGroupIfExist(string loanProductGroupCode)
        {
            var MLoanProductGroupData = context.M_LoanProductGroup
                                              .Where(s => s.ProductGroupCode == loanProductGroupCode)
                                              .FirstOrDefault();

            if (MLoanProductGroupData != null)
            {
                return MLoanProductGroupData.Id;
            }
            else
            {
                return 0;
            }

        }

        public static int AddNPLDataFileLogRecord(NplDataFileLog nplDataFileLogRecord)
        {
            using (var NPLContext = new DBContext.DocumentUploadContext())
            {
                NPLContext.NplDataFileLog.Add(nplDataFileLogRecord);
                NPLContext.SaveChanges();
            }

            if (nplDataFileLogRecord.Id > 0)
            {
                return nplDataFileLogRecord.Id;
            }
            else
            {
                return 0;
            }
        }

        //public static async System.Threading.Tasks.Task<string> MoveFileToHistoryAsync(string FTPUser, string FTPPassword, string sourcePath, string PathToMove)
        //{
        //    //var FTPUser = Configuration["UploadDocument:FTPUser"];
        //    //var FTPPassword = Configuration["UploadDocument:FTPUserPassword"];

        //    try
        //    {
        //        //Create FTP Request.
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(PathToMove);
        //        request.Method = WebRequestMethods.Ftp.DownloadFile;

        //        //Enter FTP Server credentials.
        //        request.Credentials = new NetworkCredential(FTPUser, FTPPassword);
        //        request.UsePassive = false;
        //        request.UseBinary = false;
        //        request.EnableSsl = false;

        //        //Fetch the Response and read it into a MemoryStream object.
        //        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
        //        using (Stream responseStream = response.GetResponseStream())
        //        {
        //            using (Stream fileStream = new FileStream(@"c:\UserName\Images\", FileMode.Create))
        //            {
        //                responseStream.CopyTo(fileStream);
        //            }
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        throw new Exception((ex.Response as FtpWebResponse).StatusDescription);
        //    }
        //}
    }
}

// Format and display the TimeSpan value.
//string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
//    ts.Hours, ts.Minutes, ts.Seconds,
//    ts.Milliseconds / 10);