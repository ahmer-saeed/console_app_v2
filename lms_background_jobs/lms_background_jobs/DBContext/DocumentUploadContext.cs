﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace DBModel.DBContext
{
    public class DocumentUploadContext : DbContext
    {
        public DbSet<Defaulter> Defaulter { get; set; }
        public DbSet<DefaulterAccount> DefaulterAccount { get; set; }
        public DbSet<DefaulterDetail> DefaulterDetail { get; set; }
        public DbSet<DefaulterFinance> DefaulterFinance { get; set; }

        public DbSet<LoanProduct> LoanProduct { get; set; }
        public DbSet<LoanProductGroupDetail> LoanProductGroupDetail { get; set; }
        public DbSet<LoanProductTypeDetail> LoanProductTypeDetail { get; set; }

        public DbSet<M_LoanProductGroup> M_LoanProductGroup { get; set; }
        public DbSet<M_LoanProductType> MLoanProductType { get; set; }

        public DbSet<NplDataFileLog> NplDataFileLog { get; set; }
        public DbSet<NplTemp> NplTemp { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //dev
            optionsBuilder.UseSqlServer(@"Server=skillorbit.cfbnszl7qz1s.ap-southeast-1.rds.amazonaws.com,1433;Initial Catalog=lcms_dev_v5;Integrated Security=False;User ID=skillorbit123;Password=Sskillorbit!123456;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;");

            //production
            //optionsBuilder.UseSqlServer(@"Server=10.10.48.157;Initial Catalog=lcms_dev_v7;Integrated Security=False;User ID=lms;Password=lcm$2019;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;");

            //uat
            //optionsBuilder.UseSqlServer(@"Server=UATAPPDB;Initial Catalog=lcms_dev_v7;Integrated Security=False;User ID=sa;Password=inbox@123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;");
        }
    }
}
